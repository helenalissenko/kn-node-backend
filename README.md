# kn-node-backend

The backend part for *kn-ts-frontend* project. This needs to be run before running the frontend part.
## To run the application:
  1. run `npm install`
  2. run `node index.js`
  3. run the frontend project

