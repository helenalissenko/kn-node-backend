var Express = require('express');
var multer = require('multer');
var bodyParser = require('body-parser');
var path = require('path')
var fs = require('fs');
var Validator = require('jsonschema').Validator;
var find = require('lodash').find;
var findIndex = require('lodash').findIndex;

var app = Express();
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

var uploadInfo = [];
var loggedInUser = '';
var currentlyUploadedFileName = '';
var originalFileName = '';
var currentContents = {};

var Storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, "./files");
    },
    filename: function(req, file, callback) {
        originalFileName = file.originalname;
        currentlyUploadedFileName = file.fieldname + "_" + Date.now() + "_" + originalFileName;
        callback(null, currentlyUploadedFileName);
    }
});

var upload = multer({
    storage: Storage
}).single("jsonUpload"); //Field name and max count

app.get("/uploads", function(req, res) {
    res.json({ uploads: uploadInfo });
});
app.post("/upload", async function(req, res) {
    upload(req, res, function(err) {
        if (err) {
            res.sendStatus(400);
        }
        if (!isJsonFile(currentlyUploadedFileName)) {
            deleteFile();
            res.send(500).send({ error: 'Only json files can be uploaded!' });
        } 
        else if (!isJsonFileValid()) {
            console.log('structure');
            deleteFile();
            res.send(500).send({ error: 'Json file structure is invalid' });
        } 
        else {
            saveUploadData();
            res.sendStatus(200);
        }
    });
});
app.get('/uploads/:uploadName', function(req, res) {
    var uploadName = req.params.uploadName;
    var upload = find(uploadInfo, { 'fileName': `${uploadName}.json` });
    return res.json({ upload });
});
app.post('/update/:uploadName', function(req, res) {
    var uploadName = req.params.uploadName;
    var location = req.body;
    var locationToAdd = {lat: Number(location.lat), long: Number(location.long)};

    var uploadIndex = findIndex(uploadInfo, (info) => info.fileName === uploadName);
    var upload = uploadInfo[uploadIndex];
    const parsedContents = JSON.parse(upload.contents);

    parsedContents.locations.push(locationToAdd);
    upload.contents = JSON.stringify(parsedContents);

    uploadInfo[uploadIndex] = upload;

    res.sendStatus(200);
});
app.post("/saveUsername", function(req, res) {
    loggedInUser = req.body.username;
    res.sendStatus(200);
});
app.listen(3030, function(a) {
    console.log("Listening to port 3030");
});

function isJsonFile() {
    return path.extname(currentlyUploadedFileName) === '.json';
}

async function isJsonFileValid() {
    await fs.readFile('./files/' + currentlyUploadedFileName, 'utf8', function(err, contents) {
        var validator = new Validator(schema);
        var isValid = validator.validate(contents, schema).valid;
        if (isValid) {
            currentContents = contents;
        }
        return isValid;
    });
}

function saveUploadData() {
    fs.readFile('./files/' + currentlyUploadedFileName, 'utf8', function(err, contents) {
        var uploadData = {
            user: loggedInUser,
            fileName: currentlyUploadedFileName,
            contents
        };
        uploadInfo.push(uploadData);
    });
}

function deleteFile() {
    fs.unlinkSync('./files/' + currentlyUploadedFileName);
}

var schema = {
    "locations": {
        "type": "array",
        "oneOf": [
          {
            "type": "object",
            "properties": {
              "long": {
                "type": "number"
              },
              "lat": {
                "type": "number"
              }
            },
            "required": [
              "long",
              "lat"
            ],
            "additionalProperties": false
          }
        ]
      }
  };